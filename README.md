# UUID package for Go language

[![Build Status](https://travis-ci.org/satori/go.uuid.svg?branch=master)](https://travis-ci.org/satori/go.uuid)
[![Coverage Status](https://coveralls.io/repos/github/satori/go.uuid/badge.svg?branch=master)](https://coveralls.io/github/satori/go.uuid)
[![GoDoc](http://godoc.org/github.com/satori/go.uuid?status.svg)](http://godoc.org/github.com/satori/go.uuid)

复刻自 github/satori/go.uuid


此包提供了通用唯一标识符（UUID）的纯Go实现。支持UUID的创建和解析。
具有100%的测试覆盖率和开箱即用的基准。
支持的版本：
*版本1，基于时间戳和MAC地址（RFC 4122）
*版本2，基于时间戳、MAC地址和POSIX UID/GID（DCE 1.1）
*版本3，基于MD5哈希（RFC 4122）
*版本4，基于随机数（RFC 4122）
*版本5，基于SHA-1哈希（RFC 4122）
## Installation

Use the `go` command:

	$ go get gitee.com/li0shang/go.uuid

## Requirements

UUID package tested against Go >= 1.6.

## Example

```go
package main

import (
	"fmt"
	uuid "gitee.com/li0shang/go.uuid"
)

func main() {
	// Creating UUID Version 4
	// panic on error
	u1 := uuid.Must(uuid.NewV4())
	fmt.Printf("UUIDv4: %s\n", u1)

	// or error handling
	u2, err := uuid.NewV4()
	if err != nil {
		fmt.Printf("Something went wrong: %s", err)
		return
	}
	fmt.Printf("UUIDv4: %s\n", u2)

	// Parsing UUID from string input
	u2, err := uuid.FromString("6ba7b810-9dad-11d1-80b4-00c04fd430c8")
	if err != nil {
		fmt.Printf("Something went wrong: %s", err)
		return
	}
	fmt.Printf("Successfully parsed: %s", u2)
}
```

## Documentation

[Documentation](http://godoc.org/github.com/satori/go.uuid) is hosted at GoDoc project.

## Links
* [RFC 4122](http://tools.ietf.org/html/rfc4122)
* [DCE 1.1: Authentication and Security Services](http://pubs.opengroup.org/onlinepubs/9696989899/chap5.htm#tagcjh_08_02_01_01)

## Copyright

Copyright (C) 2013-2018 by Maxim Bublis <b@codemonkey.ru>.

UUID package released under MIT License.
See [LICENSE](https://github.com/satori/go.uuid/blob/master/LICENSE) for details.
